import {
    fetch,
    post
} from '@/config/httpConfig.js'


export const getTestData = () => {
    return fetch('/api/test_get')
}

export const postTest = (data) => {
    return post('/api/test_post', data)
}

export const getOverlapData = (params) => {
    return fetch('/api/overlap', params)
}