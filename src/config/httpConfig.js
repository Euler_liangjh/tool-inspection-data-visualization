import axios from "axios";
import qs from "qs";
// 默认超时设置
axios.defaults.timeout = 60000;

axios.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded; charset=utf-8;";
//http request 拦截器
axios.interceptors.request.use(
  (config) => {
    if (config.method === "post") {
      if (config.headers["Content-Type"] === "multipart/form-data") {
        console.log(config);
        console.log("form data");
      } else {
        config.data = qs.stringify(config.data);
      }
    }
    return config;
  },
  (err) => {
    return Promise.reject(err);
  }
);
//http response 拦截器
axios.interceptors.response.use(
  (response) => {
    console.log("response==>", response);
    let { status, data } = response;
    if (status !== 200 && status !== 201 && status !== 204) {
      console.log("网络异常，请刷新或者重试!");
      return Promise.reject("网络异常!");
    }
    return data;
  },
  (error) => {
    return Promise.reject(error);
  }
);

/**
 * 封装get方法
 * @param url
 * @param params
 * @returns {Promise}
 */
export function fetch(url, params = {}) {
  return new Promise((resolve, reject) => {
    axios
      .get(url, {
        params: params,
      })
      .then((data) => {
        if (data.code === 0) {
          resolve(data.data);
        } else {
          console.log("get 请求错误: " + data.msg);
        }
      })
      .catch((err) => {
        reject(err);
        let message = "请求失败！请检查网络";
        console.log(message);
      });
  });
}

/**
 * 封装post请求
 * @param url
 * @param data
 * @returns {Promise}
 */

export function post(url, data) {
  return new Promise((resolve, reject) => {
    axios.post(url, data).then(
      (response) => {
        console.log("post==>", response);
        if (response.code === 0) {
          resolve(response.data);
        } else {
          resolve("error");
        }
      },
      (err) => {
        reject(err);
        let message = "请求失败！请检查网络";
        if (err.response) message = err.response.data.message;
        console.log(message);
      }
    );
  });
}

/**
 * 封装patch请求
 * @param url
 * @param data
 * @returns {Promise}
 */

export function patch(url, data = {}) {
  return new Promise((resolve, reject) => {
    axios.patch(url, data).then(
      (response) => {
        if (response.data.code === 200) {
          resolve(response.data.data);
        } else {
          console.log(response.data.msg);
        }
      },
      (err) => {
        reject(err);
        let message = "请求失败！请检查网络";
        if (err.response) message = err.response.data.message;
        console.log(message);
      }
    );
  });
}

/**
 * 封装put请求
 * @param url
 * @param data
 * @returns {Promise}
 */

export function put(url, data = {}) {
  return new Promise((resolve, reject) => {
    axios.put(url, data).then(
      (response) => {
        if (response.data.code === 200) {
          resolve(response.data.data);
        } else {
          console.log(response.data.msg);
        }
      },
      (err) => {
        reject(err);
        let message = "请求失败！请检查网络";
        if (err.response) message = err.response.data.message;
        console.log(message);
      }
    );
  });
}

export function del(url, data = {}) {
  return new Promise((resolve, reject) => {
    axios.delete(url, data).then(
      (response) => {
        if (response.data.code === 200) {
          resolve(response.data.data);
        } else {
          console.log(response.data.msg);
        }
      },
      (err) => {
        reject(err);
        let message = "请求失败！请检查网络";
        if (err.response) message = err.response.data.message;
        console.log(message);
      }
    );
  });
}
